/**
 * Класс, объекты которого описывают параметры гамбургера.
 *
 * @constructor
 * @param size        Размер
 * @param stuffing    Начинка
 * @throws {HamburgerException}  При неправильном использовании
 */
function Hamburger(size, stuffing) {
  if (!size || size.type != "SIZE") {
    if (!size) {
      this._message = "HamburgerException: no size given";
    } else {
      this._message =
        "HamburgerException: invalid size '" +
        size.type +
        "_" +
        size.name +
        "'";
    }
  } else {
    this._size = size;
  }
  if (!stuffing || stuffing.type != "STUFFING") {
    if (!stuffing) {
      this._message =
        (this._message ? this._message + "; " : "") +
        "HamburgerException: no stuffing given";
    } else {
      this._message =
        (this._message ? this._message + "; " : "") +
        "HamburgerException: invalid stuffing '" +
        stuffing.type +
        "_" +
        stuffing.name +
        "'";
    }
  } else {
    this._stuffing = stuffing;
  }

  if (this._message) {
    return new HamburgerException(this._message);
  }
}

/* Размеры, виды начинок и добавок */
function FeatureHamburger(type, name, price, calorific) {
  this.type = type;
  this.name = name;
  this.price = price;
  this.calorific = calorific;
}
Hamburger.SIZE_SMALL = new FeatureHamburger("SIZE", "SMALL", 50, 20);
Hamburger.SIZE_LARGE = new FeatureHamburger("SIZE", "LARGE", 100, 40);
Hamburger.STUFFING_CHEESE = new FeatureHamburger("STUFFING", "CHEESE", 10, 20);
Hamburger.STUFFING_SALAD = new FeatureHamburger("STUFFING", "SALAD", 20, 5);
Hamburger.STUFFING_POTATO = new FeatureHamburger("STUFFING", "POTATO", 15, 10);
Hamburger.TOPPING_MAYO = new FeatureHamburger("TOPPING", "MAYO", 20, 5);
Hamburger.TOPPING_SPICE = new FeatureHamburger("TOPPING", "SPICE", 15, 0);

/**
 * Добавить добавку к гамбургеру. Можно добавить несколько
 * добавок, при условии, что они разные.
 *
 * @param topping     Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.addTopping = function(topping) {
  if (!topping || topping.type != "TOPPING") {
    if (!topping) {
      var exception = "HamburgerException: no topping given";
    } else {
      var exception =
        "HamburgerException: invalid topping '" +
        topping.type +
        "_" +
        topping.name +
        "'";
    }
  } else {
    if (!this._toppings) {
      this._toppings = [topping];
    } else {
      for (var tmpTopping of this._toppings) {
        if (tmpTopping == topping) {
          var exception =
            "HamburgerException: topping '" +
            topping.type +
            "_" +
            topping.name +
            "' was added";
        }
      }
      if (!exception) {
        this._toppings.push(topping);
      }
    }
  }

  if (exception) {
    this._message = (this._message ? this._message + "; " : "") + exception;
    return new HamburgerException(exception);
  }
};

/**
 * Убрать добавку, при условии, что она ранее была
 * добавлена.
 *
 * @param topping   Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.removeTopping = function(topping) {
  var index = this._toppings.indexOf(topping);
  if (index != -1) {
    this._toppings.splice(index, 1);
  }
};

/**
 * Получить список добавок.
 *
 * @return {Array} Массив добавленных добавок, содержит константы
 *                 Hamburger.TOPPING_*
 */
Hamburger.prototype.getToppings = function() {
  return this._toppings;
};

/**
 * Узнать размер гамбургера
 */
Hamburger.prototype.getSize = function() {
  return this._size;
};

/**
 * Узнать начинку гамбургера
 */
Hamburger.prototype.getStuffing = function() {
  return this._stuffing;
};

/**
 * Узнать цену гамбургера
 * @return {Number} Цена в тугриках
 */
Hamburger.prototype.calculatePrice = function() {
  var toppingsPrice = 0;
  for (var tmpTopping of this._toppings) {
    toppingsPrice = toppingsPrice + tmpTopping.price;
  }
  return this._size.price + this._stuffing.price + toppingsPrice;
};

/**
 * Узнать калорийность
 * @return {Number} Калорийность в калориях
 */
Hamburger.prototype.calculateCalories = function() {
  var toppingsCalorific = 0;
  for (var tmpTopping of this._toppings) {
    toppingsCalorific = toppingsCalorific + tmpTopping.calorific;
  }
  return this._size.calorific + this._stuffing.calorific + toppingsCalorific;
};

/**
 * Представляет информацию об ошибке в ходе работы с гамбургером.
 * Подробности хранятся в свойстве message.
 * @constructor
 */
function HamburgerException(message) {
  this._message = message;
}

// маленький гамбургер с начинкой из сыра
var hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
console.log(hamburger);

// добавка из майонеза
hamburger.addTopping(Hamburger.TOPPING_MAYO);
console.log(hamburger);
// спросим сколько там калорий
console.log("Calories: %f", hamburger.calculateCalories());
// сколько стоит
console.log("Price: %f", hamburger.calculatePrice());
// я тут передумал и решил добавить еще приправу
hamburger.addTopping(Hamburger.TOPPING_SPICE);
console.log(hamburger);
// А сколько теперь стоит?
console.log("Price with sauce: %f", hamburger.calculatePrice());
// Проверить, большой ли гамбургер?
console.log(
  "Is hamburger large: %s",
  hamburger.getSize() === Hamburger.SIZE_LARGE
); // -> false
// Убрать добавку
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log(hamburger);
console.log("Have %d toppings", hamburger.getToppings().length); // 1

console.log(hamburger.getStuffing());

// не передали обязательные параметры
var h2 = new Hamburger(); // => HamburgerException: no size given
console.log(h2);

// передаем некорректные значения, добавку вместо размера
var h3 = new Hamburger(Hamburger.TOPPING_SPICE, Hamburger.TOPPING_SPICE);
// => HamburgerException: invalid size 'TOPPING_SAUCE'
console.log(h3);

// добавляем много добавок
var h4 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
hamburger.addTopping(Hamburger.TOPPING_MAYO);
var t4 = hamburger.addTopping(Hamburger.TOPPING_MAYO);
// HamburgerException: duplicate topping 'TOPPING_MAYO'
console.log(t4);
